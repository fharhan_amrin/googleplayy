<!-- light-blue - v4.0.1 - 2018-10-31 -->

<!DOCTYPE html>
<html>

<head>
    <title>Voucher Google Play</title>

    <link href="css/application.css" rel="stylesheet">
    <link href="css/toastr.css" rel="stylesheet">

    <!-- DataTables -->
    <link href="plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" media="all">
    <link href="plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" media="all" type="text/css" />
    <link href="plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" media="all" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" media="all" type="text/css" />

    <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="css/loader.css">
    <link rel="shortcut icon" href="img/google_play.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script>
        /* yeah we need this empty stylesheet here. It's cool chrome & chromium fix
           chrome fix https://code.google.com/p/chromium/issues/detail?id=167083
                      https://code.google.com/p/chromium/issues/detail?id=332189
        */
    </script>
</head>

<body>

    <div class="content container ">
        <h2 id="titlePage" class="page-title">Detail Voucher Game <small id="subtitlePage">Management</small></h2>
        <div class="konten element-animation">
            <section class="widget">
                <header>
                    <h4>
                        Google Play Voucher List
                        <small>

                        </small>
                    </h4>
                    <div class="widget-controls">
                        <a title="Options" href="#"><i class="glyphicon glyphicon-cog"></i></a>
                        <a data-widgster="expand" title="Expand" href="#"><i class="glyphicon glyphicon-chevron-up"></i></a>
                        <a data-widgster="collapse" title="Collapse" href="#"><i class="glyphicon glyphicon-chevron-down"></i></a>
                        <a data-widgster="close" title="Close" href="#"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </header>
                <div class="body">
                    <!-- <div class="button-group">
                    <button class="btn-primary btn" onclick="openModal('add')"><i class="glyphicon glyphicon-plus"></i>
                        Add Voucher</button>
                    <button class="btn-default btn" data-toggle="modal" data-target="#modalFormExcel"><i
                            class="glyphicon glyphicon-upload"></i> Upload Voucher (Excel)</button>
                </div> -->

                    <div class="table-responsive well">
                        <form action="javascript:void(0);" method="GET" id="frmFilter">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="msisdn" class="control-label">
                                            msisdn
                                        </label>
                                        <!-- <select name="denom" class="form-control" id="denom">
                                        <option value="67073838268">67073838268</option>
                                        <option value="6707383826">67073838268</option>
                                        <option value="67073838268">67073838268</option>
                                       
                                    </select> -->
                                        <input type="text" class="form-control" name="msisdn" id="msisdn" placholder="msisdn">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="periodStart" class="control-label">
                                            Start Date Purchase
                                        </label>
                                        <input type="date" class="form-control" name="periodStart" id="periodStart">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label for="periodStart" class="control-label">
                                            End Date Purchase
                                        </label>
                                        <input type="date" class="form-control" name="periodEnd" id="periodEnd">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <button style="margin-top:22px;" class="btn btn-primary" type="submit">Filter</button>
                                        <button style="margin-top:22px;" class="btn btn-default" onclick="getData()" type="reset">Reset</button>
                                        <!-- <button class="btn btn-success">Import Excel</button>    -->
                                    </div>
                                </div>
                            </div>
                        </form>

                        <table class="table table-hover dataTableCustom">
                            <thead>
                                <tr>
                                    <!-- <th>#</th> -->
                                    <th>date</th>
                                    <th>msisdn</th>
                                    <th>refillId</th>
                                    <th>amount</th>
                                    <th>responseCode</th>
                                    <th>sourceChannel</th>
                                    <!-- <th>purchaseStatus</th> -->
                                    <th>purchaseName</th>
                                    <th>code</th>
                                    <th>employeeStatus</th>
                                    <th>Type</th>

                                </tr>
                            </thead>
                            <tbody id="tblBody">

                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>

        <footer class="content-footer">
            Games Dashboard - Made by <a href="https://shiblysolution.com" rel="nofollow noopener noreferrer" target="_blank">STS</a>
        </footer>
    </div>
    <div class="loader-wrap hiding hide">
        <i class="fa fa-circle-o-notch fa-spin"></i>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora, quo aut totam quod veritatis quasi consequuntur. Ut modi dignissimos, obcaecati placeat quasi nihil distinctio perspiciatis nemo expedita eius aspernatur reiciendis?

    </div>
    </div>


    <!-- common libraries. required for every page-->
    <script src="lib/jquery/dist/jquery.min.js"></script>
    <script src="lib/jquery-pjax/jquery.pjax.js"></script>
    <script src="lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
    <script src="lib/widgster/widgster.js"></script>
    <script src="lib/underscore/underscore.js"></script>
    <script src="js/toastr.min.js"></script>
    <!-- Required datatable js -->
    <script src="sweetalert2/sweetalert2.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="plugins/datatables/jszip.min.js"></script>
    <script src="plugins/datatables/pdfmake.min.js"></script>
    <script src="plugins/datatables/vfs_fonts.js"></script>
    <script src="plugins/datatables/buttons.html5.min.js"></script>
    <script src="plugins/datatables/buttons.print.min.js"></script>
    <!-- End plugin js for this page-->

    <!-- common application js -->
    <!-- <script src="js/app.js?id=24Apr2019"></script> -->
    <script src="js/settings.js"></script>

    <!-- common templates -->
    <script type="text/template" id="settings-template">
        <div class="setting clearfix">
        <div>Sidebar on the</div>
        <div id="sidebar-toggle" class="pull-left btn-group" data-toggle="buttons-radio">
            <% onRight = sidebar == 'right'%>
            <button type="button" data-value="left" class="btn btn-sm btn-default <%= onRight? '' : 'active' %>">Left</button>
            <button type="button" data-value="right" class="btn btn-sm btn-default <%= onRight? 'active' : '' %>">Right</button>
        </div>
    </div>
    <div class="setting clearfix">
        <div>Sidebar</div>
        <div id="display-sidebar-toggle" class="pull-left btn-group" data-toggle="buttons-radio">
            <% display = displaySidebar%>
            <button type="button" data-value="true" class="btn btn-sm btn-default <%= display? 'active' : '' %>">Show</button>
            <button type="button" data-value="false" class="btn btn-sm btn-default <%= display? '' : 'active' %>">Hide</button>
        </div>
    </div>
</script>

    <script type="text/template" id="sidebar-settings-template">
        <% auto = sidebarState == 'auto'%>
    <% if (auto) {%>
    <button type="button"
            data-value="icons"
            class="btn-icons btn btn-transparent btn-sm">Icons</button>
    <button type="button"
            data-value="auto"
            class="btn-auto btn btn-transparent btn-sm">Auto</button>
    <%} else {%>
    <button type="button"
            data-value="auto"
            class="btn btn-transparent btn-sm">Auto</button>
    <% } %>
</script>

    <!-- page specific scripts -->
    <!-- page libs -->
    <script src="lib/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="lib/jquery.sparkline/index.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>

    <script src="lib/backbone/backbone.js"></script>
    <script src="lib/backbone.localStorage/build/backbone.localStorage.min.js"></script>

    <script src="lib/d3/d3.min.js"></script>

    <script src="plugins/moment.js"></script>
    <script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="lib/nvd3/build/nv.d3.min.js"></script>
    <script src="js/link.js"></script>


    <!-- page template -->
    <script type="text/template" id="message-template">
        <div class="sender pull-left">
                <div class="icon">
                    <img src="img/2.png" class="img-circle" alt="">
                </div>
                <div class="time">
                    just now
                </div>
            </div>
            <div class="chat-message-body">
                <span class="arrow"></span>
                <div class="sender"><a href="#">Tikhon Laninga</a></div>
                <div class="text">
                    <%- text %>
                </div>
            </div>
        </script>

    <script>
        $(document).ready(function() {
            getData();

            $("#frmFilter").submit(function() {
                $.ajax({
                    url: api_url + 'DataVoucherCdr/getVoucherGooglePlayDetail',
                    data: {
                        msisdn: $("#msisdn").val(),
                        stDate: $("#periodStart").val(),
                        endDate: $("#periodEnd").val()
                    },
                    method: 'GET',
                    dataType: 'json',
                    beforeSend: function() {
                        $(".dataTableCustom").DataTable().destroy();
                        $("#tblBody").html(
                            '<tr><td colspan="10"><center><div class="lds-grid"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></center></td></tr>'
                        );
                    },
                    success: function(dt) {
                        if (dt.success) {
                            setDataTable(dt.data);
                        } else {
                            returnErrorMessage(dt.msg);
                        }
                    }
                })
            })

        });

        function setDataTable(data) {
            // var no = 1,
            tblBody = $("#tblBody");
            tblBody.html('');
            for (var x in data) {
                var el = data[x]
                //   stat = el.status,
                //   statText;

                // var button = "";
                // if (stat == 1) {
                //     statText = "<label class='label label-danger'>Used</label>";
                // } else if (stat == 0) {
                //     button += '<button class="btn btn-default" onclick="getDataEdit(\'' + el.serialNumber +
                //         '\')"> <i class="glyphicon glyphicon-pencil"></i></button>' +
                //         '<button onclick="deleteVoucher(\'' + el.serialNumber +
                //         '\')" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i></button>';
                //     statText = "<label class='label label-success'>Available</label>";
                // }
                // statVoucher = "<label class='label label-success'>Melon</label>";
                // if (el.status_voucher == 0) {
                //     statVoucher = "<label class='label label-warning'>Non Melon</label>";
                //    }
                var tbRow = '<tr>' +
                    // '<td>' + no + '</td>' +
                    '<td>' + el.date + '</td>' +
                    '<td>' + el.msisdn + '</td>' +
                    '<td>' + el.refillId + '</td>' +
                    '<td>' + el.amount + '</td>' +
                    '<td>' + el.responseCode + '</td>' +
                    '<td>' + el.sourceChannel + '</td>' +
                    // '<td>' + el.purchaseStatus + '</td>' +
                    '<td>' + el.purchaseName + '</td>' +
                    '<td>' + el.code + '</td>' +
                    '<td>' + el.employeeStatus + '</td>' +
                    '<td>' + el.type + '</td>' +
                    //  '<td>' + statText + '</td>' +
                    // '<td>' + statVoucher + '</td>' +
                    // '<td>' + button + '</td>' +
                    '</tr>';
                tblBody.append(tbRow);
                // no++;
            }


            runDataTable();
        }

        function runDataTable(param) {
            var params = 0;
            if (param) {
                params = param;
            }
            if ($.fn.dataTable.isDataTable(".dataTableCustom")) {
                $('.dataTableCustom').DataTable({
                    "info": false,
                    "paging": true,
                    dom: 'Bfrtip',
                    order: [
                        [params, "asc"]
                    ],

                    buttons: [{
                        extend: 'excel',
                        text: 'Export excel',
                        className: 'exportExcel btn btn-default btn-sm pull-right',
                        filename: 'Data Voucher',
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    }],
                });
            } else {
                $('.dataTableCustom').DataTable({
                    "info": false,
                    "paging": true,
                    dom: 'Bfrtip',
                    order: [
                        [params, "asc"]
                    ],

                    buttons: [{
                        extend: 'excel',
                        text: 'Export excel',
                        className: 'exportExcel btn btn-success btn-sm pull-right',
                        filename: 'Data Voucher',
                        exportOptions: {
                            modifier: {
                                page: 'all'
                            }
                        }
                    }],
                });
            }
        }

        function getData() {
            $.ajax({
                url: api_url + "DataVoucherCdr/getVoucherGooglePlayDetail",
                method: 'GET',
                dataType: 'json',
                beforeSend: function() {
                    $(".dataTableCustom").DataTable().destroy();
                    $("#tblBody").html(
                        '<tr><td colspan="10"><center><div class="lds-grid"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></center></td></tr>'
                    );
                },
                success: function(dt) {
                    if (dt.success) {
                        setDataTable(dt.data);
                    } else {
                        returnErrorMessage(dt.msg);
                    }
                }
            })
        }
       
    </script>

</body>

</html>