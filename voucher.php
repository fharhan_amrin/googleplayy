<!-- light-blue - v4.0.1 - 2018-10-31 -->

<!DOCTYPE html>
<html>

<head>
    <title>Voucher Google Play</title>

    <link href="css/application.css" rel="stylesheet">
    <link href="css/toastr.css" rel="stylesheet">

    <!-- DataTables -->
    <link href="plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" media="all">
    <link href="plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" media="all" type="text/css" />
    <link href="plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" media="all" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" media="all" type="text/css" />

    <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
    <link rel="shortcut icon" href="img/google_play.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script>
    /* yeah we need this empty stylesheet here. It's cool chrome & chromium fix
           chrome fix https://code.google.com/p/chromium/issues/detail?id=167083
                      https://code.google.com/p/chromium/issues/detail?id=332189
        */
    </script>
</head>

<body>

    <div class="content container ">
        <!-- <h2 id="titlePage" class="page-title">Voucher Game <small id="subtitlePage">Management</small></h2> -->
        <div class="konten element-animation">

            <div class="row">
                <div class="col-lg-12">
                    <!-- <section class="widget">
                        <div class="row">
                            <div class="col-lg-6">
                                <div id="container"
                                    style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div id="containerDetail"
                                    style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">
                                </div>
                            </div>
                        </div>
                    </section> -->
                    <section class="widget">
                        <header>
                            <!-- <h4>
                                Google Play Voucher List
                                <small>

                                </small>
                            </h4> -->
                            <div class="widget-controls">
                                <a title="Options" href="#"><i class="glyphicon glyphicon-cog"></i></a>
                                <a data-widgster="expand" title="Expand" href="#"><i
                                        class="glyphicon glyphicon-chevron-up"></i></a>
                                <a data-widgster="collapse" title="Collapse" href="#"><i
                                        class="glyphicon glyphicon-chevron-down"></i></a>
                                <a data-widgster="close" title="Close" href="#"><i
                                        class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </header>
                        <div class="body">
                            <!-- <div class="button-group">
                                <button class="btn-primary btn" onclick="openModal('add')"><i class="glyphicon glyphicon-plus"></i>
                                    Add Voucher</button>
                                <button class="btn-default btn" data-toggle="modal" data-target="#modalFormExcel"><i class="glyphicon glyphicon-upload"></i> Upload Voucher (Excel)</button>
                            </div> -->

                            <div class="table-responsive well">
                                
                            
                                <!-- awal coba form start data -->
                                <form action="javascript:void(0);" method="GET" id="frmFilter">
                                    <div class="row">
                                        
                                       <div class="col-md-2">
                                           <label for="startdate" class="control-label">Start Date</label>
                                           <input type="date" name="startdate" id="startdate" class="form-control"> 
                                       </div>

                                       <div class="col-md-2">
                                           <label for="enddate" class="control-label">End Date</label>
                                           <input type="date"  name="enddate" id="enddate" class="form-control"> 
                                       </div>
                                       
                                        <div class="col-md-2">
                                            <div class="form-group" style="position: relative;top: 20px;">
                                                <button style="position: relative;top:3px;" class="btn btn-primary"
                                                    type="submit">Go</button>
                                                <!-- <button style="margin-top:22px;" class="btn btn-default" onclick="apa()"
                                                    type="reset">Reset</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!-- akhir coba form enddate -->
                                
                                

                                <div class="row" style="margin-top: 10px;">
                                    <input type="radio" name="gender" value="male"> Revenue Based
                                    <!-- <?php echo date("l jS \of F Y  "); ?><br> -->
                                    <!-- <label class="txt-white" style="margin-left: 10px;">Revenue Based</label> -->
                                    <button type="button" class="pull-right btn btn-primary" data-toggle="collapse"
                                        data-target="#demo">Show</button>

                                    <div id="demo" class="collapse">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div id="donat"></div>
                                            </div>

                                            <div class="col-md-6">
                                                <div id="basicbar"
                                                    style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="margin-top: 20px;">
                                            <div id="detail"
                                                style="min-width: 310px; max-width: 1000px; height: 400px; margin: 0 auto">
                                            </div>

                                        </div>


                                    </div>
                                    <hr>
                                    <!-- collapse ke dua -->
                                    
                                    <label class="txt-white" style="margin-left: 10px;">Transaction : TPS </label>
                                    <button type="button" class="pull-right btn btn-primary" data-toggle="collapse"
                                        data-target="#dua">Show</button>
                                    <div id="dua" class="collapse">

                                        <div class="row" class="mt-4">
                                            <div id="detail_tps"
                                                style="min-width: 310px; max-width: 1000px; height: 400px; margin: 0 auto">
                                            </div>

                                        </div>


                                    </div>
                                    <hr>
                                    <!-- collapse ke tiga -->
                                    <label class="txt-white" style="margin-left: 10px;">Transaction : SUCCESS RATE
                                    </label>

                                    <button type="button" class="pull-right btn btn-primary" data-toggle="collapse"
                                        data-target="#tiga">Show</button>
                                    <div id="tiga" class="collapse">
                                    <div class="row">                                        <div class="col-md-6">
                                            <div id="donat_tiga" style="height: 400px"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="basicbar_tiga"
                                                style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto">
                                            </div>
                                        </div>
                                        </div>
                                        <div class="row" style="margin-top:36px;">
                                            <div id="detail_tiga"
                                                style="min-width: 310px; max-width: 1000px; height: 400px; margin: 0 auto">
                                            </div>

                                        </div>


                                    </div>
                                    <hr>
                                    <!-- collpase ke empat -->

                                    <label class="txt-white" style="margin-left: 10px;">Transaction : Error & Success
                                        Non Rev
                                    </label>

                                    <button type="button" class="pull-right btn btn-primary" data-toggle="collapse"
                                        data-target="#tiga">Show</button>
                                    <div id="tiga" class="collapse">
                                        <div class="col-md-6">
                                            <div id="donat_tiga" style="height: 400px"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <div id="basicbar_tiga"
                                                style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto">
                                            </div>
                                        </div>
                                        <div class="row" class="mt-4">
                                            <div id="detail_tiga"
                                                style="min-width: 310px; max-width: 1000px; height: 400px; margin: 0 auto">
                                            </div>

                                        </div>


                                    </div>
                                </div>
                            </div>

                        </div>

                    </section>

                </div>
            </div>
            <!-- akhir row -->


        </div>

        <footer class="content-footer">
            Games Dashboard - Made by <a href="https://shiblysolution.com" rel="nofollow noopener noreferrer"
                target="_blank">STS</a>
        </footer>
    </div>
    <div class="loader-wrap hiding hide">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>
    </div>


    <!-- common libraries. required for every page-->
    <script src="lib/jquery/dist/jquery.min.js"></script>
    <script src="lib/jquery-pjax/jquery.pjax.js"></script>
    <script src="lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
    <script src="lib/widgster/widgster.js"></script>
    <script src="lib/underscore/underscore.js"></script>
    <script src="js/toastr.min.js"></script>
    <!-- Required datatable js -->
    <script src="sweetalert2/sweetalert2.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="plugins/datatables/jszip.min.js"></script>
    <script src="plugins/datatables/pdfmake.min.js"></script>
    <script src="plugins/datatables/vfs_fonts.js"></script>
    <script src="plugins/datatables/buttons.html5.min.js"></script>
    <script src="plugins/datatables/buttons.print.min.js"></script>
    <!-- End plugin js for this page-->

    <!-- common application js -->
    <script src="js/settings.js"></script>

    <!-- common templates -->

    <!-- page specific scripts -->
    <!-- page libs -->
    <script src="lib/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="lib/jquery.sparkline/index.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>

    <script src="lib/backbone/backbone.js"></script>
    <script src="lib/backbone.localStorage/build/backbone.localStorage.min.js"></script>

    <script src="lib/d3/d3.min.js"></script>

    <script src="plugins/moment.js"></script>
    <script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="lib/nvd3/build/nv.d3.min.js"></script>

    <script src="js/link.js"></script>

    <!-- page template -->


    <script type="text/javascript">
    // alert("hello wordl");

    $(document).ready(function() {
        apa();
        filter();
        donat_tiga();
        detail();
        detail_success_rate();
        basicbar();
        basicbar_tiga();
        detail_tps();
    });

    function filter() {
        $('#frmFilter').on('submit', function() {
            // alert("oke");
            // var tanggal = $('#tanggal').val();
            // var bulan = $('#bulan').val();
            // var tahun = $('#tahun').val();
            // var per = $('#per').val();
            // apa(tanggal, bulan, tahun, per);
            // donat_tiga(tanggal, bulan, tahun, per);
            // basicbar(tanggal, bulan, tahun, per);
            // basicbar_tiga(tanggal, bulan, tahun, per);
            // detail(tanggal, bulan, tahun, per);
            // detail_tps(tanggal, bulan, tahun, per);
            // detail_success_rate(tanggal, bulan, tahun, per);
            var startdate = $('#startdate').val();
            var enddate = $('#enddate').val();
            apa(startdate,enddate);
            donat_tiga(startdate,enddate);
            basicbar(startdate,enddate);
            basicbar_tiga(startdate,enddate);
            detail(startdate,enddate);
            detail_tps(startdate,enddate);
            detail_success_rate(startdate,enddate);


        });
    }

    // ini 
    function apa(startdate = '', enddate = '') {
    //    var tahun = $('#tahun').val();
    //     var bulan = $('#bulan').val();
    //     var tanggal = $('#tanggal').val();
           var startdate = $('#startdate').val();
           var enddate = $('#enddate').val();
        
        var hasil =  startdate + " => " + enddate;
        $.ajax({
            type: "GET",
            url: api_url + 'DataVoucherCdr/coba?startdate=' + startdate + '&enddate=' + enddate,
            dataType: "JSON",
            success: function(response) {

                Highcharts.chart('donat', {
                    colors: ['#58d53f', '#fae819', '#f20707'],
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '<b>Voucher Games Success Rate <br> </b>'
                    },
                    subtitle: {
                        text: '<b>'+'('+hasil+')'+'</b>'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}'
                            },
                            showInLegend: true
                        }

                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: response
                    }]
                });
                // akhir highchart
            }
        });
    }


    //basicbar

    function basicbar(startdate,enddate) {
         var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        // var tanggal = $('#tanggal').val();
        
        var hasil =  startdate + " => " + enddate ;
        
        $.ajax({
            type: "GET",
            url: api_url + 'DataVoucherCdr/basicbar?startdate=' + startdate + '&enddate=' + enddate,
            dataType: "JSON",

            success: function(response) {

                Highcharts.chart('basicbar', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: '<b>Voucher Games Top Revenue</b>'
                    },
                    subtitle: {
                         text: '<b>'+'('+hasil+')'+'</b>'
                    },
                    xAxis: {
                        categories: response.refillId,
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Population (millions)',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: '$'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            },
                            showInLegend: false
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 80,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme
                            .legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'bulan',
                        data: response.total_pendapatan
                    }]
                });
            }
        });
    }

    // detail
    function detail(startdate,enddate) {
          var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        // var tanggal = $('#tanggal').val();
        
        var hasil =  startdate + " => " + enddate ;
        $.ajax({
            type: "GET",
            url: api_url + 'DataVoucherCdr/detail?startdate=' + startdate + '&enddate=' + enddate,
            dataType: "JSON",
            success: function(response) {
                // debugger;
                Highcharts.chart('detail', {
                    title: {
                        text: '<b>Voucher Games Revenue</b>'
                    },
                    subtitle: {
                         text: '<b>'+'('+hasil+')'+'</b>'
                    },

                    xAxis: {
                        categories: response.tanggal
                    },

                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        data: response.total
                    }]
                });
            }
        });
    }


    function detail_tps(startdate,enddate) {
         var startdate = $('#startdate').val();
         var enddate = $('#enddate').val();
        
        var hasil =  startdate + " => " + enddate ;
        $.ajax({
            type: "GET",
            url: api_url + 'DataVoucherCdr/detail_tps?startdate=' + startdate + '&enddate=' + enddate,
            dataType: "JSON",
            success: function(response) {
                // debugger;
                Highcharts.chart('detail_tps', {
                    title: {
                        text: '<b>Voucher Games TPS</b>'
                    },
                    subtitle: {
                         text: '<b>'+'('+hasil+')'+'</b>'
                    },

                    xAxis: {
                        categories: response.tanggal
                    },

                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        data: response.total_pendapatan
                    }]
                });
            }
        });
    }



    // collapse tiga success Rate
    // donat_tiga
    // Build the chart
   function donat_tiga(startdate,enddate) {
       var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        // var tanggal = $('#tanggal').val();
        
        var hasil =  startdate + " => " + enddate;
        $.ajax({
            type: "GET",
            url: api_url + 'DataVoucherCdr/coba?startdate=' + startdate + '&enddate=' + enddate,
            dataType: "JSON",
            success: function(response) {

                Highcharts.chart('donat_tiga', {
                    colors: ['#58d53f', '#fae819', '#f20707'],
                    chart: {
                        type: 'pie',
                        options3d: {
                            enabled: true,
                            alpha: 45,
                            beta: 0
                        }
                    },
                    title: {
                        text: '<b>Voucher Games Success Rate <br> </b>'
                    },
                    subtitle: {
                        text: '<b>'+'('+hasil+')'+'</b>'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            depth: 35,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}'
                            },
                            showInLegend: true
                        }

                    },
                    series: [{
                        name: 'Brands',
                        colorByPoint: true,
                        data: response
                    }]
                });
                // akhir highchart
            }
        });
    }
    //basicbar_tiga

      function basicbar_tiga(startdate,enddate) {
        //  var tahun = $('#tahun').val();
        // var bulan = $('#bulan').val();
        // var tanggal = $('#tanggal').val();
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        
        var hasil =  startdate + " => " + enddate;
        
        $.ajax({
            type: "GET",
            url: api_url + 'DataVoucherCdr/basicbar_tiga?startdate=' + startdate + '&enddate=' + enddate,
            dataType: "JSON",

            success: function(response) {

                Highcharts.chart('basicbar_tiga', {
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: '<b>Voucher Games Top Revenue</b>'
                    },
                    subtitle: {
                         text: '<b>'+'('+hasil+')'+'</b>'
                    },
                    xAxis: {
                        categories: response.refillId,
                        title: {
                            text: null
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Population (millions)',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        valueSuffix: '$'
                    },
                    plotOptions: {
                        bar: {
                            dataLabels: {
                                enabled: true
                            },
                            showInLegend: false
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 80,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: ((Highcharts.theme && Highcharts.theme
                            .legendBackgroundColor) || '#FFFFFF'),
                        shadow: true
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'bulan',
                        data: response.total_pendapatan
                    }]
                });
            }
        });
    }
   



    // detail
     function detail_success_rate(startdate,enddate) {
        var startdate = $('#startdate').val();
        var enddate = $('#enddate').val();
        
        var hasil =  startdate + " => " + enddate ;
        $.ajax({
            type: "GET",
            url: api_url + 'DataVoucherCdr/detail_success_rate?startdate=' + startdate + '&enddate=' + enddate,
            dataType: "JSON",
            success: function(response) {
                // debugger;
                Highcharts.chart('detail_tiga', {
                    title: {
                        text: '<b>Voucher Games Success Rate ALL</b>'
                    },
                    subtitle: {
                         text: '<b>'+'('+hasil+')'+'</b>'
                    },

                    xAxis: {
                        categories: response.tanggal
                    },

                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        data: response.total_pendapatan
                    }]
                });
            }
        });
    }
    
    </script>
</body>

</html>