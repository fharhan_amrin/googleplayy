<!-- light-blue - v4.0.1 - 2018-10-31 -->

<!DOCTYPE html>
<html>

<head>
    <title>Voucher Google Play</title>

    <link href="css/application.css" rel="stylesheet">
    <link href="css/toastr.css" rel="stylesheet">

    <!-- DataTables -->
    <link href="plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" media="all">
    <link href="plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" media="all" type="text/css" />
    <link href="plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" media="all" type="text/css" />
    <!-- Responsive datatable examples -->
    <link href="plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" media="all" type="text/css" />

    <link rel="stylesheet" href="sweetalert2/sweetalert2.min.css">
    <link rel="shortcut icon" href="img/google_play.png">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta charset="utf-8">
    <script>
    /* yeah we need this empty stylesheet here. It's cool chrome & chromium fix
           chrome fix https://code.google.com/p/chromium/issues/detail?id=167083
                      https://code.google.com/p/chromium/issues/detail?id=332189
        */
    </script>
</head>

<body>

    <div class="content container ">
        <!-- <h2 id="titlePage" class="page-title">Voucher Game <small id="subtitlePage">Management</small></h2> -->
        <div class="konten element-animation">

            <div class="row">
                <div class="col-lg-12">
                    <!-- <section class="widget">
                        <div class="row">
                            <div class="col-lg-6">
                                <div id="container"
                                    style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div id="containerDetail"
                                    style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">
                                </div>
                            </div>
                        </div>
                    </section> -->
                    <section class="widget">
                        <header>
                            <!-- <h4>
                                Google Play Voucher List
                                <small>

                                </small>
                            </h4> -->
                            <div class="widget-controls">
                                <a title="Options" href="#"><i class="glyphicon glyphicon-cog"></i></a>
                                <a data-widgster="expand" title="Expand" href="#"><i
                                        class="glyphicon glyphicon-chevron-up"></i></a>
                                <a data-widgster="collapse" title="Collapse" href="#"><i
                                        class="glyphicon glyphicon-chevron-down"></i></a>
                                <a data-widgster="close" title="Close" href="#"><i
                                        class="glyphicon glyphicon-remove"></i></a>
                            </div>
                        </header>
                        <div class="body">
                         

                            <div class="table-responsive well">
                                
                            
                                <!-- awal coba form start data -->
                             
                                <!-- akhir coba form enddate -->
                                
                                

                               <div class="row">
                                <div class="row" style="margin-top: 20px;">
                                            <div id="detail"
                                                style="min-width: 310px; max-width: 1000px; height: 400px; margin: 0 auto">
                                            </div>

                                        </div>
                             
                               </div>
                            </div>

                        </div>

                    </section>

                </div>
            </div>
            <!-- akhir row -->


        </div>

        <footer class="content-footer">
            Games Dashboard - Made by <a href="https://shiblysolution.com" rel="nofollow noopener noreferrer"
                target="_blank">STS</a>
        </footer>
    </div>
    <div class="loader-wrap hiding hide">
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>
    </div>


    <!-- common libraries. required for every page-->
    <script src="lib/jquery/dist/jquery.min.js"></script>
    <script src="lib/jquery-pjax/jquery.pjax.js"></script>
    <script src="lib/bootstrap-sass/assets/javascripts/bootstrap.min.js"></script>
    <script src="lib/widgster/widgster.js"></script>
    <script src="lib/underscore/underscore.js"></script>
    <script src="js/toastr.min.js"></script>
    <!-- Required datatable js -->
    <script src="sweetalert2/sweetalert2.min.js"></script>
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <!-- Buttons examples -->
    <script src="plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="plugins/datatables/buttons.bootstrap4.min.js"></script>
    <script src="plugins/datatables/jszip.min.js"></script>
    <script src="plugins/datatables/pdfmake.min.js"></script>
    <script src="plugins/datatables/vfs_fonts.js"></script>
    <script src="plugins/datatables/buttons.html5.min.js"></script>
    <script src="plugins/datatables/buttons.print.min.js"></script>
    <!-- End plugin js for this page-->

    <!-- common application js -->
    <script src="js/settings.js"></script>

    <!-- common templates -->

    <!-- page specific scripts -->
    <!-- page libs -->
    <script src="lib/slimScroll/jquery.slimscroll.min.js"></script>

    <script src="lib/jquery.sparkline/index.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="https://code.highcharts.com/modules/data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>

    <script src="lib/backbone/backbone.js"></script>
    <script src="lib/backbone.localStorage/build/backbone.localStorage.min.js"></script>

    <script src="lib/d3/d3.min.js"></script>

    <script src="plugins/moment.js"></script>
    <script src="plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="lib/nvd3/build/nv.d3.min.js"></script>

    <script src="js/link.js"></script>

    <!-- page template -->

  <script>

    $(document).ready(function () {
        detail();
        // alert("hello world");
    });
      function detail() {
        
        
        
        $.ajax({
            type: "GET",
            url: api_url + 'Ussd/latihan',
            dataType: "JSON",
            success: function(response) {
                // debugger;
                Highcharts.chart('detail', {
                    title: {
                        text: '<b>USSD</b>'
                    },
                    // subtitle: {
                    //      text: 'hello'
                    // },

                    xAxis: {
                        categories: response.tanggal
                    },

                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    },

                    series: [{
                        name: 'USSD',
                        data: response.total
                    }]
                });
            }
        });
    }

  </script>
    
</body>

</html>